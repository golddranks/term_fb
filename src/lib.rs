#![feature(duration_float)]
#![feature(range_contains)]

mod term;
mod sim_loop;
mod fb;
mod buf;
mod input_parser;
mod utils;

pub use crate::term::{TermOut, TermIn};
pub use crate::sim_loop::SimLoop;
pub use crate::buf::Buf;
pub use crate::fb::FrameBuf;
pub use crate::term::get_default_term;
pub use crate::input_parser::{InputKey, InputKeyIter};
pub use crate::utils::{xy, xy_coord, Coord, Vec2};
