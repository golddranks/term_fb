use std::ops::{Add, AddAssign, Sub};

pub fn xy(x: isize, y: isize) -> Vec2 {
    Vec2 { x, y }
}

pub fn xy_coord(x: usize, y: usize) -> Coord {
    Coord { x, y }
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Vec2 {
    pub x: isize,
    pub y: isize,
}

impl Vec2 {
    pub fn coord(self) -> Coord {
        Coord {
            x: self.x as usize,
            y: self.y as usize,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Coord {
    pub x: usize,
    pub y: usize,
}

impl Coord {
    pub fn inside_square_inc(self, top_left: Coord, bottom_right: Coord) -> bool {
        if self.x < top_left.x { return false };
        if self.y < top_left.y { return false };
        if self.x >= bottom_right.x { return false };
        if self.y >= bottom_right.y { return false };

        true
    }
}

impl Add<Vec2> for Coord {
    type Output = Coord;

    fn add(mut self, other: Vec2) -> Coord {
        self.x = (self.x as isize + other.x) as usize;
        self.y = (self.y as isize + other.y) as usize;
        self
    }
}

impl Sub<Vec2> for Coord {
    type Output = Coord;

    fn sub(mut self, other: Vec2) -> Coord {
        self.x = (self.x as isize - other.x) as usize;
        self.y = (self.y as isize - other.y) as usize;
        self
    }
}

impl AddAssign<Vec2> for Coord {

    fn add_assign(&mut self, other: Vec2) {
        self.x = (self.x as isize + other.x) as usize;
        self.y = (self.y as isize + other.y) as usize;
    }
}

