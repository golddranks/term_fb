use std::cmp::max;
use std::mem;
use crate::{Coord, xy_coord};

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Buf {
    store: Vec<char>,
    stride: usize,
    pub used: bool,
    pub quit: bool,
    pub show_fps: bool,
}

trait FillSliceExt {
    type Item;
    fn fill_with<I: Iterator<Item=Self::Item>>(&mut self, iter: I);
}

impl<T> FillSliceExt for [T] {
    type Item = T;

    fn fill_with<I: Iterator<Item=Self::Item>>(&mut self, iter: I) {
        for (item, slot) in iter.zip(self.into_iter()) {
            *slot = item;
        }
    }
}


impl Buf {
    const EMPTY: [char; 256] = [' '; 256];

    pub fn height(&self) -> usize {
        if self.stride > 0 { self.store.len() / self.stride } else { 0 }
    }

    pub fn from_str(s: &str) -> Self {
        let (stride, count) = s.lines()
            .fold((0, 0), |(stride, count), line| {
                (max(stride, line.len()), count + 1)
            });
        let mut store = Vec::with_capacity(stride * count);
        for line in s.lines() {
            let old_len = store.len();
            store.extend(line.chars());
            let space_left = stride - (store.len() - old_len);
            store.extend_from_slice(&Buf::EMPTY[0..space_left]);
        }

        Self {
            store,
            stride,
            used:  false,
            quit: false,
            show_fps: false,
        }
    }

    pub fn from_lines(s: &[&str]) -> Self {
        let stride = s.iter().fold(0, |stride, line| max(stride, line.len()));
        let mut store = Vec::with_capacity(stride * s.len());
        for line in s {
            let old_len = store.len();
            store.extend(line.chars());
            let space_left = stride - (store.len() - old_len);
            store.extend_from_slice(&Buf::EMPTY[0..space_left]);
        }

        Self {
            store,
            stride,
            used:  false,
            quit: false,
            show_fps: false,
        }
    }

    pub fn empty() -> Self {
        Self {
            store: Vec::new(),
            stride: 0,
            used:  false,
            quit: false,
            show_fps: false,
        }
    }

    pub fn new(width: usize, height: usize, character: char) -> Self {
        let mut store = Vec::new();
        store.resize(width * height, character);

        Self {
            store,
            stride: width,
            used:  false,
            quit: false,
            show_fps: false,
        }
    }

    pub fn blit(&mut self, character: char, coord: Coord) {
        self.store[coord.y * self.stride + coord.x] = character;
    }

    pub fn blit_line(&mut self, character: char, mut start: Coord, mut end: Coord) {
        let vertical;
        let mut main;
        let end_main;
        let mut side;
        let end_side;
        if isize::abs(end.x as isize - start.x as isize) < isize::abs(end.y as isize - start.y as isize) {
            main = start.y;
            end_main = end.y;
            side = start.x;
            end_side = end.x;
            vertical = true;
        } else {
            main = start.x;
            end_main= end.x;
            side = start.y;
            end_side = end.y;
            vertical = false;
        }
        let main_step = if main < end_main { 1 } else { -1 };
        let side_step = if side < end_side { 1 } else { -1 };
        let mut error = 0;
        let delta_err = isize::abs((end_side as isize - side as isize) * 2);
        let err_threshold = isize::abs(end_main as isize - main as isize);

        // Drawing loop
        while main != end_main {
            if vertical {
                self.blit(character, xy_coord(side, main as usize));
            } else {
                self.blit(character, xy_coord(main as usize, side));
            }
            main = (main as isize + main_step) as usize;
            error += delta_err;
            if error >= err_threshold {
                side = (side as isize + side_step) as usize;
                error -= err_threshold * 2;
            }
        }
        if vertical {
            self.blit(character, xy_coord(side, end_main));
        } else {
            self.blit(character, xy_coord(end_main, side));
        }
    }

    // TODO: decide what to do with multiline text
    pub fn blit_text(&mut self, text: &str, coord: Coord) -> Coord {
        let start_index = coord.y * self.stride + coord.x;
        let end_index = start_index + (self.stride - coord.x);

        self.store[start_index..end_index].fill_with(text.chars());

        Coord { x: coord.x + text.len(), y: coord.y }

    }

    pub fn blit_int(&mut self, int: impl itoa::Integer, coord: Coord) -> Coord {
        let mut buf = itoa::Buffer::new();
        let text = buf.format(int);
        let start_index = coord.y * self.stride + coord.x;
        let end_index = start_index + (self.stride - coord.x);

        self.store[start_index..end_index].fill_with(text.chars());

        Coord { x: coord.x + text.len(), y: coord.y }
    }

    pub fn blit_float(&mut self, float: impl ryu::Float, precision: usize, coord: Coord) -> Coord {
        let mut buf = ryu::Buffer::new();
        let text = buf.format(float);
        let end = text.find('.')
            .unwrap_or(text.len())
                + 1 + precision;
        let text = &text[..end];
        let start_index = coord.y * self.stride + coord.x;
        let end_index = start_index + text.len();

        self.store[start_index..end_index].fill_with(text.chars());

        Coord { x: coord.x + text.len(), y: coord.y }
    }

    pub fn copy_from(&mut self, other: &Buf) {
        self.store.clear();
        self.store.extend_from_slice(&other.store);
        self.stride = other.stride;
        self.show_fps = other.show_fps;
        self.quit = other.quit;
        self.used = other.used;
    }

    pub fn lines(&self) -> impl Iterator<Item=&[char]> {
        self.store.chunks(max(self.stride, 1))
    }
}

#[test]
fn test_from_str_1() {
    let got = Buf::from_str("test");
    let expected = Buf {
        store: Vec::from("test".as_bytes()),
        stride: 4,
        used:  false,
        quit: false,
        show_fps: false,
    };
    assert_eq!(expected, got);
}

#[test]
fn test_from_str_2() {
    let got = Buf::from_str("test1\ntest2");
    let expected = Buf {
        store: Vec::from("test1test2".as_bytes()),
        stride: 5,
        used:  false,
        quit: false,
        show_fps: false,
    };
    assert_eq!(expected, got);
}

#[test]
fn test_from_str_3() {
    let got = Buf::from_str("test1\nt2\ntest3");
    let expected = Buf {
        store: Vec::from("test1t2   test3".as_bytes()),
        stride: 5,
        used:  false,
        quit: false,
        show_fps: false,
    };
    assert_eq!(expected, got);
}
