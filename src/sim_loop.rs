use std::thread;
use std::sync::Arc;
use std::thread::JoinHandle;
use std::io;
use std::error::Error as StdError;

use spin_sleep::LoopHelper;
use crossbeam::atomic::AtomicCell;

use crate::{Buf, TermIn, TermOut, FrameBuf, xy, InputKeyIter};

pub struct SimLoop {
    buf_cell: Arc<AtomicCell<Box<Buf>>>,
    loop_helper: LoopHelper,
    term_in: TermIn,
    render_guard: JoinHandle<Result<FrameBuf, io::Error>>,
}

impl SimLoop {
    fn render_loop(term_out: TermOut, buf_cell: Arc<AtomicCell<Box<Buf>>>, render_fps: f64) -> io::Result<FrameBuf> {

        let mut framebuf = FrameBuf::new(term_out);
        let mut buf = Box::new(Buf::empty());
        let mut loop_helper = LoopHelper::builder().build_with_target_rate(render_fps);

        loop {
            let loop_duration = loop_helper.loop_start();
            buf = buf_cell.swap(buf);
            if buf.quit {
                return Ok(framebuf);
            }
            if buf.show_fps {
                let fps_xy = buf.blit_text("Render FPS ", xy(0, 0).coord());
                buf.blit_float(1.0/loop_duration.as_float_secs(), 1, fps_xy);
            }
            buf = framebuf.display(buf)?;
            loop_helper.loop_sleep();
        }
    }

    pub fn new(sim_fps: f64, render_fps: f64, term_in: TermIn, term_out: TermOut) -> io::Result<SimLoop> {

        let buf_cell_sim = Arc::new(AtomicCell::new(Box::new(Buf::empty())));
        let buf_cell_render = buf_cell_sim.clone();

        let render_guard = thread::spawn(move|| {
            Self::render_loop(term_out, buf_cell_render, render_fps)
        });

        Ok(Self {
            buf_cell: buf_cell_sim,
            loop_helper: LoopHelper::builder().build_with_target_rate(sim_fps),
            term_in,
            render_guard,
        })
    }

    pub fn ready(&mut self, output: Box<Buf>) -> Result<(InputKeyIter<'_>, Box<Buf>), Box<dyn StdError>> {

        let used_buf = self.buf_cell.swap(output);
        self.loop_helper.loop_sleep();
        self.loop_helper.loop_start();

        Ok((self.term_in.read_input()?, used_buf))
    }

    pub fn cleanup(self, mut output: Box<Buf>) -> Result<(), Box<dyn StdError>> {
        output.quit = true;
        self.buf_cell.swap(output);
        let framebuf = self.render_guard.join()
            .map_err(|e| format!("Render thread panicked: {:?}", e))??;
        framebuf.cleanup()?;
        Ok(())
    }
}
