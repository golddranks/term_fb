use std::{mem, io};

use crate::{TermOut, Buf};


pub struct FrameBuf {
    term: TermOut,
    current_buf: Box<Buf>,
}

impl FrameBuf {
    pub fn new(term: TermOut) -> Self {

        Self {
            term,
            current_buf: Box::new(Buf::empty()),
        }
    }

    pub fn display(&mut self, mut buf: Box<Buf>) -> io::Result<Box<Buf>> {

        if !buf.used {
            self.term.write_frame(&mut buf)?;
            mem::swap(&mut buf,&mut self.current_buf);
            self.current_buf.used = true;
        }

        Ok(buf)
    }

    pub fn cleanup(mut self) -> io::Result<()> {
        self.term.cleanup(&self.current_buf)?;
        Ok(())
    }
}
