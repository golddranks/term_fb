# A frame buffer for terminals.

Contains an event loop with input parsing and a char-based buffer abstraction for output.

NOTE! This library is still in a very early phase! API is subject to change.

## To try

Run in terminal:

```
cargo run --examples oni
```

## To use

Add to `Cargo.toml`:
```
[dependencies]
term_fb = "0.0.1"
```
